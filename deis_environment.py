from lab.environments import SlurmEnvironment

class DEISSlurmEnvironment(SlurmEnvironment):
    """Environment for DEIS cluster."""

    DEFAULT_PARTITION = "naples" # Alternatives are dhabi or rome
    DEFAULT_QOS = "normal"
    DEFAULT_MEMORY_PER_CPU = "4096M"
    MAX_TASKS = 2000 - 1  # see slurm.conf
    NICE_VALUE = 0

    DEFAULT_EXPORT = []
